<?php
class AchievementMilestones_CPT {

	public $parent;

	function __construct($parent) { 
		$this->parent = $parent;
		add_action('init', array($this, 'register_cpt'));
		add_action('admin_init', array($this, 'admin_init'));
	}

	function register_cpt() {
		$result = register_post_type( 'dpa_milestone', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
		 	// let's now add all the options for this post type
			array('labels' => array(
				'name' => __('Achievement Milestones', 'dpa_milestone general name'), /* This is the Title of the Group */
				'singular_name' => __('Achievement Milestone', 'dpa_milestone singular name'), /* This is the individual type */
				'all_items' => __('All Achievement Milestones'), /* the all items menu item */
				'add_new' => __('Add New', 'custom dpa_milestone item'), /* The add new menu item */
				'add_new_item' => __('Add New Achievement Milestone'), /* Add New Display Title */
				'edit' => __( 'Edit' ), /* Edit Dialog */
				'edit_item' => __('Edit Achievement Milestone'), /* Edit Display Title */
				'new_item' => __('New Achievement Milestone'), /* New Display Title */
				'view_item' => __('View Achievement Milestone'), /* View Display Title */
				'search_items' => __('Search Achievement Milestone'), /* Search Achievement Milestone Title */ 
				'not_found' =>  __('Nothing found in the Database.'), /* This displays if there are no entries yet */ 
				'not_found_in_trash' => __('Nothing found in Trash'), /* This displays if there is nothing in the trash */
				'parent_item_colon' => ''
				), /* end of arrays */
				'description' => __( 'Stores achievement milestones in the database' ), /* Achievement Milestone Description */
				'public' => true,
				'publicly_queryable' => false,
				'exclude_from_search' => true,
				'show_ui' => true,
				'query_var' => true,
				'menu_position' => 8, /* this is what order you want it to appear in on the left hand side menu */ 
				// 'menu_icon' => get_stylesheet_directory_uri() . '/library/images/custom-post-icon.png', /* the icon for the custom post type menu */
				'rewrite'	=> array( 'slug' => 'dpa_milestone', 'with_front' => false ), /* you can specify it's url slug */
				'has_archive' => 'dpa_milestone', /* you can rename the slug here */
				'capability_type' => 'post',
				'hierarchical' => true,
				/* the next one is important, it tells what's enabled in the post editor */
				'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'custom-fields', 'comments', 'page-attributes')
		 	) /* end of options */
		); /* end of register post type */

	}

	function admin_init() {
		add_meta_box('dpa_milestone_achievements_meta', 'Select Achievements', array($this, 'dpa_milestone_achievements_meta_box'), 'dpa_milestone', 'normal', 'high', $callback_args = null);
	}

	function dpa_milestone_achievements_meta_box() {
		$achievements = $this->get_achievements();
		foreach ($achievements['achievements'] as $key => $achievement) {
			echo '<input type="checkbox" value="'.$achievement->id.'" id="achievement-'.$achievement->id.'">';
			echo '<label for="achievement-'.$achievement->id.'">'.$achievement->name.'</label>';
			echo '<br />';
		}
	}


	function get_achievements() {
		$args = array(
			'limit' => $max,
			'page' => $this->pag_page,
			'per_page' => $this->pag_num,
			'populate_extras' => $populate_extras,
			'search_terms' => $search_terms,
			'skip_detail_page_result' => $skip_detail_page_result,
			'slug' => $slug,
			'action' => $action,
			'type' => $type,
			'user_id' => $user_id
		);
		$achievements = DPA_Achievement::get( $args );
		return $achievements;
	}

}	

?>